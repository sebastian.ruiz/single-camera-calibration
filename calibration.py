#!/usr/bin/env python
import numpy as np
import cv2
import os
import argparse
import yaml
from glob import glob
import time
import regex as re


def get_images(input_dir):
    images = [img for img in os.listdir(input_dir) if img.endswith(".png")]
    images.sort(key=lambda f: int(re.sub('\D', '', f)))
    images = [os.path.join(input_dir, image) for image in images]
    return images


def resize(input, resolution=(1024, 768), calibration=None, undistort=False, crop=False, crop_margins=(50, 400, 50, 50), output=None):
    if calibration is not None:
        with open(calibration) as fr:
            c = yaml.load(fr)
    if isinstance(input, list):
        for input_img in input:
            img = cv2.imread(input_img)
            if img is None:
                print("Failed to load " + input_img)
                continue
            img = cv2.resize(img, resolution, interpolation=cv2.INTER_AREA)
            t0 = time.time()
            if undistort:
                K_undistort = np.array(c['camera_matrix'])
                img = cv2.undistort(img, np.array(c['camera_matrix']), np.array(c['dist_coefs']),
                                        newCameraMatrix=K_undistort)

            if crop:
                # crop image
                margin_top, margin_right, margin_bottom, margin_left = crop_margins
                img = img[margin_top:-1 - margin_bottom, margin_left:-1 - margin_right]
                # cv2.namedWindow('cropped', cv2.WINDOW_NORMAL)
                # cv2.imshow("cropped", img)
                # cv2.waitKey(0)
                # return  # debug

            t1 = time.time()
            print("time taken:", t1 - t0)

            name, ext = os.path.splitext(os.path.basename(input_img))
            cv2.imwrite(os.path.join(output, name + ext), img)


def calibrate(input, output, pattern_size=(8, 6), debug_dir=None, framestep=20):

    pattern_points = np.zeros((np.prod(pattern_size), 3), np.float32)
    pattern_points[:, :2] = np.indices(pattern_size).T.reshape(-1, 2)
    # pattern_points *= square_size

    obj_points = []
    img_points = []
    h, w = 0, 0
    i = -1

    if isinstance(input, list):
        # assume folder of png files.
        source = input
    else:
        source = cv2.VideoCapture(input)

    while True:
        i += 1
        if isinstance(source, list):
            # glob
            if i == len(source):
                break
            img = cv2.imread(source[i])
        else:
            # cv2.VideoCapture
            retval, img = source.read()
            if not retval:
                break
            if i % framestep != 0:
                continue

        print('Searching for chessboard in frame ' + str(i) + '...'),
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        h, w = img.shape[:2]
        found, corners = cv2.findChessboardCorners(img, pattern_size, flags=cv2.CALIB_CB_FILTER_QUADS)
        if found:
            term = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_COUNT, 30, 0.1)
            cv2.cornerSubPix(img, corners, (5, 5), (-1, -1), term)
        if debug_dir:
            img_chess = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
            cv2.drawChessboardCorners(img_chess, pattern_size, corners, found)
            cv2.imwrite(os.path.join(debug_dir, '%04d.png' % i), img_chess)
        if not found:
            print('not found')
            continue
        img_points.append(corners.reshape(1, -1, 2))
        obj_points.append(pattern_points.reshape(1, -1, 3))

        print('ok')

    print('\nPerforming calibration...')
    rms, camera_matrix, dist_coefs, rvecs, tvecs = cv2.calibrateCamera(obj_points, img_points, (w, h), None, None)
    print("RMS:", rms)
    print("camera matrix:\n", camera_matrix)
    print("distortion coefficients: ", dist_coefs.ravel())

    calibration = {'rms': rms, 'camera_matrix': camera_matrix.tolist(), 'dist_coefs': dist_coefs.tolist()}
    output_file = output
    if os.path.isdir(output):
        output_file = os.path.join(output, "calibration.yaml")
    with open(output_file, 'w') as fw:
        yaml.dump(calibration, fw)


def undistort(input, calibration, output):
    with open(calibration) as fr:
        c = yaml.load(fr)

    for fn in input:
        t0 = time.time()
        print('processing %s...' % fn)
        img = cv2.imread(fn)
        if img is None:
            print("Failed to load " + fn)
            continue

        print('Original Dimensions : ', img.shape)

        K_undistort = np.array(c['camera_matrix'])
        img_und = cv2.undistort(img, np.array(c['camera_matrix']), np.array(c['dist_coefs']),
                                newCameraMatrix=K_undistort)

        name, ext = os.path.splitext(os.path.basename(fn))
        cv2.imwrite(os.path.join(output, name + ext), img_und)

        print('ok')
        t1 = time.time()
        print("time diff:", t1 - t0)

if __name__ == '__main__':
    scale_factor = 1/2
    resolution = (int(4608 * scale_factor), int(3288 * scale_factor))
    ################################
    # Calibration images           #
    ################################

    # step 1: resize to resolution
    # resize(get_images("data/calibration/0_input"), resolution=resolution, output="data/calibration/1_input_resized_2k")

    # step 2: calibrate on resized images
    # calibrate(get_images("data/calibration/1_input_resized_2k"), "data/calibration/1_input_resized_2k/calibration_2k.yaml")

    # step 3: undistort resized images
    # undistort(get_images("data/calibration/1_input_resized_2k"), "data/calibration/1_input_resized_2k/calibration_2k.yaml", "data/calibration/2_output_2k_undistorted")

    # step 4: calibrated resized images
    # calibrate(get_images("data/calibration/2_output_2k_undistorted"), "data/calibration/2_output_2k_undistorted/calibration_2k_undistorted.yaml")

    ################################
    # dataset images               #
    ################################

    # step 1: resize to resolution
    # step 2: undistort resized images
    # step 3: crop
    resize(get_images("data/kalo_dataset_1/0_input"),
           resolution=resolution,
           calibration="data/calibration/1_input_resized_2k/calibration_2k.yaml",
           undistort=True,
           crop=True,
           crop_margins=(50, 400, 50, 50),
           output="data/kalo_dataset_1/1_output")
